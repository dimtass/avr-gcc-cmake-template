@echo off
rmdir /s /q build-avr
cd source
cmake -H. -B"..\build-avr" -DCMAKE_TOOLCHAIN_FILE=..\cmake\TOOLCHAIN_arm_none_eabi_cortex_m3.cmake
cd ..
make -C build-avr
@echo on
